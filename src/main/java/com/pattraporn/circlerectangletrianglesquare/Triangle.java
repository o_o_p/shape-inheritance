/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.circlerectangletrianglesquare;

/**
 *
 * @author Pattrapon N
 */
public class Triangle extends Shape{
    private double a,b;
    public static final double area = 6;
    public Triangle(double a,double b){
        System.out.println("Trangle created");
        this.a =a;
        this.b =b;
    }
    @Override
    public double CalArea(){
        return area * a*b;
    }
}
