/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pattraporn.circlerectangletrianglesquare;

/**
 *
 * @author Pattrapon N
 */
public class TestShape {
    public static void main(String[] args) {
        Shape shape = new Shape();
        System.out.println();
        Circle circle = new Circle(7);
        System.out.println(circle.CalArea());
        Triangle triangle = new Triangle(4,5);
        System.out.println(triangle.CalArea());
        Rectangle rectangle = new Rectangle(5,5);
        System.out.println(rectangle.CalArea());
        Square square = new Square(4);
        System.out.println(square.CalArea());
        
        Shape[] shapes = {circle,triangle,rectangle,square};
        for (int i = 0; i < shapes.length; i++) {
            System.out.println(shapes[i].CalArea());

    }
}}
